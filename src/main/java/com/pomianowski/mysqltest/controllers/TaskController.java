package com.pomianowski.mysqltest.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pomianowski.mysqltest.model.Task;
import com.pomianowski.mysqltest.services.TaskService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/rest/tasks")
public class TaskController {

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    private TaskService taskService;

    @GetMapping(value = "/all")
    public List<Task> getAll() {
        return taskService.findAll();
    }

    @PostMapping(value = "/done")
    public void done(@RequestBody final int id) {
    	taskService.done(id);
    }

	@PostMapping(value = "/undone")
	public void undone(@RequestBody final int id) {
		taskService.undone(id);
	}

	@PostMapping(value = "/save")
	public void save(@RequestBody final Task task) {
		taskService.save(task);
	}

    @DeleteMapping
    public void delete(@RequestBody final int id) {
        taskService.delete(id);
    }
}
