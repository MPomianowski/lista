package com.pomianowski.mysqltest.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class Task {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private Date creationDate;
    private Boolean isDone;

}
