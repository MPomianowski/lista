package com.pomianowski.mysqltest.services;

import com.pomianowski.mysqltest.model.Task;
import com.pomianowski.mysqltest.repository.TasksRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    public TaskService(TasksRepository tasksRepository) {
        this.tasksRepository = tasksRepository;
    }

    private TasksRepository tasksRepository;


    public List<Task> findAll() {
        return tasksRepository.findAll();
    }

    public Optional<Task> findById(int id) {
		return tasksRepository.findById(id);
	}

    public void save(Task task) {
        task.setCreationDate(new Date());
        task.setIsDone(false);
        tasksRepository.save(task);
    }

    public void done(int id){
		Optional<Task> opt;
		opt = tasksRepository.findById(id);
		opt.ifPresent(task -> {
			task.setIsDone(true);
			tasksRepository.save(task);
		});
	}

    public void delete(int id) {
        Optional<Task> task = tasksRepository.findById(id);
        if(task.isPresent()) {
            tasksRepository.delete(task.get());
        }
    }

	public void undone(int id) {
		Optional<Task> opt;
		opt = tasksRepository.findById(id);
		opt.ifPresent(task -> {
			task.setIsDone(false);
			tasksRepository.save(task);
		});
	}
}
