package com.pomianowski.mysqltest.repository;

import com.pomianowski.mysqltest.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TasksRepository extends JpaRepository<Task, Integer> {
}
